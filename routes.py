from flask import Blueprint,make_response,request
import base64,json,os,requests,time
#gnupg
path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
routes = Blueprint('/',__name__,url_prefix='/')

@routes.route('/auth')
def auth():
  return 'auth'
#register < post [public key]

#login < post [random string]

#recent < get
@routes.route('/recent')
def recent():
  return 'recent'

#message < post [encrypted file]

#update < fetch latest release of code base
@routes.route('/update/',defaults={'uri':''})
@routes.route('/update/<path:uri>')
def update(uri):
  res = ''
  url = path+'/auth/'+uri
  if os.path.isfile(url):
    fp = open(url,'r',encoding='utf-8')
    req = fp.read().strip()
    fp.close()
    data = requests.get(req)
    fp = open(__file__,'wb')
    for chunk in data.iter_content(100000):
      fp.write(chunk)
      fp.close()
    res = '+'
    req = '/var/www/'+request.host.replace('.','_')+'_wsgi.py'
    with open(req,'a'):
      os.utime(req,None)
  return res

@routes.route('/')
def index():
  return ''
